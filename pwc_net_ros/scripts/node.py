#!/usr/bin/env python

import rospy
from pwc_net_ros.pwc_net_ros import PwcNetRos


if __name__ == '__main__':
    rospy.init_node('pwc_net_ros')
    try:
        PwcNetRos()
    except rospy.ROSInterruptException:
        pass
    rospy.spin()