import rospy
import threading
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from pwc_net.pwc_net import PwcNet

from utils import get_optical_flow_image


class PwcNetRos():
    """ROS node to estimate optical flow from video stream using PWC-Net."""

    def __init__(self):
        self._load_param()
        self._net = PwcNet(self.pwc_weights_path)

        self._images = []
        self._timestamp = []

        self._bridge = CvBridge()
        self._images_lock = threading.Lock()

        # Publishers and subscribers
        self._optical_flow_pub = rospy.Publisher(
            'optical_flow_out', Image, queue_size=1)
        self._optical_flow_hsv_pub = rospy.Publisher(
            'optical_flow_hsv_out', Image, queue_size=1)
        self._image_sub = rospy.Subscriber(
            'image_in', Image, self._image_callback)

        # Callback for estimating optical flow
        self._estimate_optical_flow()

    def _load_param(self):
        self.rate = rospy.get_param("~rate", 10.0)
        self.pwc_weights_path = rospy.get_param("~pwc_weights")
        self.pwc_output_path = rospy.get_param("~pwc_output")
        self.optical_flow_visual_scale = rospy.get_param(
            "~optical_flow_visual_scale", 1.0)
        self.optical_flow_visual_alpha = rospy.get_param(
            "~optical_flow_visual_alpha", 1.0)

    def _image_callback(self, msg):
        try:
            image = self._bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            print(e)

        with self._images_lock:
            # Buffer incoming images
            self._images.append(image)
            self._timestamp.append(msg.header.stamp)

    def _estimate_optical_flow(self):
        r = rospy.Rate(self.rate)
        # Estimate optical flow periodically.
        while not rospy.is_shutdown():
            # Wait until there are at least two messages
            if len(self._images) < 2:
                continue

            # Acquire images
            with self._images_lock:
                image_0 = self._images[-2]
                stamp_0 = self._timestamp[-2]
                image_1 = self._images[-1]
                stamp_1 = self._timestamp[-1]
                self._images = [self._images[-1]]  # Only keep the last image in buffer
                self._timestamp = [self._timestamp[-1]]

            # Estimate optical flow
            optical_flow = self._net.run(image_0, image_1)
            flow_stamp = stamp_0 + 0.5 * (stamp_1 - stamp_0)

            # Publish optical flow
            try:
                flow_msg = self._bridge.cv2_to_imgmsg(optical_flow, "32FC2")
                flow_msg.header.stamp = flow_stamp
                self._optical_flow_pub.publish(flow_msg)
            except CvBridgeError as e:
                print(e)

            # Publish optical flow visualization
            if self._optical_flow_hsv_pub.get_num_connections() > 0:
                hsv_bgr = get_optical_flow_image(
                    optical_flow,
                    scale=self.optical_flow_visual_scale,
                    background_image=image_1,
                    blend_alpha=self.optical_flow_visual_alpha)
                try:
                    visual_msg = self._bridge.cv2_to_imgmsg(hsv_bgr, "bgr8")
                    visual_msg.header.stamp = flow_stamp
                    self._optical_flow_hsv_pub.publish(visual_msg)
                except CvBridgeError as e:
                    print(e)

            r.sleep()
