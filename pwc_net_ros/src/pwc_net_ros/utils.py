import numpy as np
import cv2

def get_optical_flow_image(optical_flow, scale=1.0, background_image=None, blend_alpha=0.75):
    """Return a BGR image to visualize optical flow using HSV color space.

    Code adapted from:
        https://www.reddit.com/r/opencv/comments/6g298p/understanding_hsv_optical_flow_visualization_with/

    Args:
        optical_flow (numpy.ndarray): Optical flow encoded as two-channel image (e.g. 32FC2)
        scale (float, optional): Scale optical flow magnitude for visualization
        image (numpy.ndarray, optional): If not None, this BGR image will be linearly blended with optical flow image.
        blend_alpha (float, optional): Parameter for linearly blending, in range [0, 1].

    No Longer Returned:
        numpy.ndarray: BGR image
    """
    h, w = optical_flow.shape[:2]
    fx, fy = optical_flow[:, :, 0], optical_flow[:, :, 1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx * fx + fy * fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang * (180 / np.pi / 2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v * scale, 255)

    flow_image = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    # Overlay optical flow image on top of original image (linear blending)
    if background_image is not None:
        flow_image = blend_alpha * flow_image + (1.0 - blend_alpha) * background_image
        flow_image = flow_image.astype(np.uint8)

    return flow_image


def get_color_wheel(size=501, scale=1.0):
    """Return a square color wheel for visualizing optical flow.

    Args:
        size (int, optional): Size of color wheel in pixel
        scale (float, optional): Scale optical flow magnitude

    Returns:
        numpy.ndarray: Color wheel as BGR image
    """
    half_size = size // 2
    optical_flow = np.empty((size, size, 2), dtype=float)
    for i in range(0, size):
        for j in range(0, size):
            optical_flow[i, j, 0] = float(i - half_size)
            optical_flow[i, j, 1] = float(j - half_size)

    return get_optical_flow_image(optical_flow, scale)
