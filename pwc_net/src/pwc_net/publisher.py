#!/usr/bin/env python
# license removed for brevity
import os

import rospy
from cv_bridge import CvBridge
from scipy.misc import imread
from pwc_net.msg import OpticalFlow
from sensor_msgs.msg import Image

from generate_flow import run_flow

# imgs = ['/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/data/frame_0010.png','/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/data/frame_0011.png']
flow_fn = '/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/data/frame_0010.flo'
prev_image = None


def publish_flow(imgs, data, dt):
    pub = rospy.Publisher('/flow', OpticalFlow, queue_size=10)
    rospy.init_node('publisher', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
      # Get time stamp
      secs = data.header.stamp.secs
      nsecs = data.header.stamp.nsecs
      curr_time = float(secs) + float(nsecs) * 1e-9

      print("running flow")
      flo = run_flow(imgs, 'data/', flow_fn)
      msg = OpticalFlow()
      msg.header.stamp.secs = secs
      msg.header.stamp.nsecs = nsecs
      msg.dt = dt
      msg.vx = flo[:, 0, 0]
      msg.vy = flo[:, 0, 1]

      pub.publish(msg)
      rate.sleep()


def callback_tracked_persons(data):
  filename = "/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/data/tracked_persons/tracked_persons_2d.txt"
  if os.path.exists(filename):
    append_write = 'a'  # append if already exists
  else:
    append_write = 'w+'  # make a new file if not
  with open(filename, append_write) as f:
    lines = []
    for box in data.boxes:
      line = ','.join([str(data.header.seq),str(box.track_id), str(box.x), str(box.y),
                       str(box.w), str(box.h), str(data.header.stamp.secs), str(data.header.stamp.nsecs)])
      lines+=[line]
      f.write(line + '\n')
  rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.header)


def callback_flow(data):
  bridge = CvBridge()
  curr_image = bridge.imgmsg_to_cv2(data)
  print("received image: type {} shape {}".format(type(curr_image), curr_image.shape))

  # Get time stamp
  secs = data.header.stamp.secs
  nsecs = data.header.stamp.nsecs
  curr_time = float(secs) + float(nsecs) * 1e-9

  global prev_image
  last_time = 0.0
  # If this is the first loop, initialize image matrices
  if prev_image is None:
    prev_image = curr_image
    last_time = curr_time
    return  # skip the rest of this loop

  # get time between images
  dt = curr_time - last_time

  publish_flow([prev_image.copy(), curr_image.copy()], data, dt)


def listener():
  # In ROS, nodes are uniquely named. If two nodes with the same
  # name are launched, the previous one is kicked off. The
  # anonymous=True flag means that rospy will choose a unique
  # name for our 'listener' node so that multiple listeners can
  # run simultaneously.
  rospy.init_node('publisher', anonymous=True)

  rospy.Subscriber("/camera/color/image_raw", Image, callback_flow)
  # rospy.Subscriber("/rwth_tracker/tracked_persons_2d", TrackedPersons2d, callback)

  # spin() simply keeps python from exiting until this node is stopped
  rospy.spin()

if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass