import glob
from scipy.misc import imshow

from flowlib import show_flow, visualize_flow, read_flow, flow_to_image

data_folder = "data/annotation_sequence_1/"

files = glob.glob(data_folder + "*.flo")

for file in files:
  flow = read_flow(file)
  img = flow_to_image(flow)
  imshow(img)