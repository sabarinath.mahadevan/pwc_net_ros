import cv2
import torch
import numpy as np
from math import ceil
import models


class PwcNet():
    """PWC-Net for optical flow estimation."""

    def __init__(self, weights_path, flow_file=None):
        self.flow_file = flow_file
        self.net = models.pwc_dc_net(weights_path)
        self.net = self.net.cuda()
        self.net.eval()

    def run(self, image_0, image_1):
        """Estimate optical flow.

        Args:
            image_0 (numpy.ndarray): First image
            image_1 (numpy.ndarray): Second image

        Returns:
            numpy.ndarray: Optical flow encoded as 32FC2
        """
        # rescale the image size to be multiples of 64
        divisor = 64.
        H, W = image_0.shape[:2]

        H_ = int(ceil(H / divisor) * divisor)
        W_ = int(ceil(W / divisor) * divisor)

        image_0 = cv2.resize(image_0, (W_, H_))
        image_1 = cv2.resize(image_1, (W_, H_))

        im_all = []
        for _i, _inputs in enumerate([image_0, image_1]):
            im_all.append(_inputs[:, :, ::-1])
            im_all[_i] = 1.0 * im_all[_i] / 255.0
            im_all[_i] = np.transpose(im_all[_i], (2, 0, 1))
            im_all[_i] = torch.from_numpy(im_all[_i])
            im_all[_i] = im_all[_i].expand(1, im_all[_i].size()[0], im_all[_i].size()[1], im_all[_i].size()[2])
            im_all[_i] = im_all[_i].float()

        im_all = torch.autograd.Variable(torch.cat(im_all, 1).cuda(), volatile=True)

        flo = self.net(im_all)
        flo = flo[0] * 20.0
        flo = flo.cpu().data.numpy()

        # scale the flow back to the input size
        flo = np.swapaxes(np.swapaxes(flo, 0, 1), 1, 2)
        u_ = cv2.resize(flo[:, :, 0], (W, H))
        v_ = cv2.resize(flo[:, :, 1], (W, H))
        u_ *= W / float(W_)
        v_ *= H / float(H_)
        flo = np.dstack((u_, v_))

        return flo


# def writeFlowFile(filename, uv):
#     """
#     According to the matlab code of Deqing Sun and c++ source code of Daniel Scharstein
#     Contact: dqsun@cs.brown.edu
#     Contact: schar@middlebury.edu
#     """
#     TAG_STRING = np.array(202021.25, dtype=np.float32)
#     if uv.shape[2] != 2:
#         sys.exit("writeFlowFile: flow must have two bands!");
#     H = np.array(uv.shape[0], dtype=np.int32)
#     W = np.array(uv.shape[1], dtype=np.int32)
#     with open(filename, 'wb') as f:
#         f.write(TAG_STRING.tobytes())
#         f.write(W.tobytes())
#         f.write(H.tobytes())
#         f.write(uv.tobytes())

# # pwc_model_fn = '/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/pwc_net.pth.tar'




# def main():
#     if len(sys.argv) > 1:
#         images_folder = sys.argv[1]
#     if len(sys.argv) > 2:
#         out_folder = sys.argv[2]
#     if len(sys.argv) > 3:
#         flow_fn = sys.argv[3]

#     if not os.path.exists(out_folder):
#         os.makedirs(out_folder)
#     imgs = tqdm(list(glob.glob(images_folder + "/*.jpg")))

#     for i in range(len(list(imgs)) - 1):
#         im_all = [imread(img) for img in [list(imgs)[i], list(imgs)[i+1]]]
#         im_all = [im[:, :, :3] for im in im_all]
#         flow_fn = out_folder + "/" + list(imgs)[i].split("/")[-1].split(".")[0] + ".flo"
#         run_flow(im_all, out_folder, flow_fn)

# if __name__ == '__main__':
#     if len(sys.argv) < 2:
#         print("Usage: python generate_flow.py <images folder> <output folder>")
#         exit()

#     main()



