#!/usr/bin/env python

import rospy
from frame_msgs.msg import TrackedPersons2d
from std_msgs.msg import String
import os
import numpy as np
import pandas as pd


data_dir = "/home/sabari/Documents/crowdbot/ucl_video_capture/scenarios/tracker_output/"
sequence = ["annotation_sequence_1"]


def callback(data):
  # Get time stamp
  secs = data.header.stamp.secs
  nsecs = data.header.stamp.nsecs
  curr_time = float(secs) + float(nsecs) * 1e-9

  for seq in sequence:
    frame_folder = data_dir + seq + "/"
    annotation_file = frame_folder + "image_info.txt"
    annotations = pd.read_csv(annotation_file, delimiter=" ")[['image_nr','stamp_sec.stamp_nsec' ]].values

  annotations = np.abs(annotations - [0,curr_time])
  image_nr = np.where(annotations[:,1] ==np.min(annotations[:,1]))[0][0]

  filename = "/home/sabari/vision/external_algorithms/pwc_net_ws/src/pwc_net/src/data/tracked_persons/tracked_persons_2d.txt"
  if os.path.exists(filename):
    append_write = 'a'  # append if already exists
  else:
    append_write = 'w+'  # make a new file if not
  with open(filename, append_write) as f:
    lines = []
    for box in data.boxes:
      line = ' '.join([str(image_nr),str(box.track_id), str(box.x), str(box.y),
                       str(box.w), str(box.h), '1 -1 -1 -1'])
      lines+=[line]
      f.write(line + '\n')
  rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.header)


def listener():
  # In ROS, nodes are uniquely named. If two nodes with the same
  # name are launched, the previous one is kicked off. The
  # anonymous=True flag means that rospy will choose a unique
  # name for our 'listener' node so that multiple listeners can
  # run simultaneously.
  rospy.init_node('subscriber', anonymous=True)

  # rospy.Subscriber("/camera/color/image_raw", Image, callback)
  rospy.Subscriber("/rwth_tracker/tracked_persons_2d", TrackedPersons2d, callback)
  # spin() simply keeps python from exiting until this node is stopped
  rospy.spin()

if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass